﻿using DiDrDe.DotNetTesteable.Application.Contracts;
using DiDrDe.DotNetTesteable.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiDrDe.DotNetTesteable.Application.Services
{
    public class BookingQueryService
        : IBookingQueryService
    {
        private readonly IRepository<Domain.Booking> _bookingRepository;
        private readonly IMapper<Domain.Booking, Booking> _bookingMapper;


        public BookingQueryService(
            IRepository<Domain.Booking> bookingRepository,
            IMapper<Domain.Booking, Booking> bookingMapper)
        {
            _bookingRepository = bookingRepository;
            _bookingMapper = bookingMapper;
        }

        public async Task<IEnumerable<Booking>> GetBookings()
        {
            var domainBookings = await _bookingRepository.GetAll();
            var bookings = domainBookings.Select(x => _bookingMapper.Map(x));
            return bookings;
        }

        public async Task<Booking> GetBooking(Guid id)
        {
            var domainBooking = await _bookingRepository.GetById(id);
            var booking = _bookingMapper.Map(domainBooking);
            return booking;
        }
    }
}