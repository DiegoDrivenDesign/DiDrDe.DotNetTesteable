﻿using DiDrDe.Database.Infra.MongoDb.Model;
using DiDrDe.DotNetTesteable.Application.Contracts;
using DiDrDe.DotNetTesteable.Domain;

namespace DiDrDe.Database.Infra.MongoDb.Mappers
{
    public class BookingToDatabaseBookingMapper
        : IMapper<Booking, DatabaseBooking>
    {
        public DatabaseBooking Map(Booking source)
        {
            var id = source.Id;
            var name = source.Name;
            var databaseBooking =
                new DatabaseBooking
                {
                    Id = id,
                    Name = name
                };
            return databaseBooking;
        }
    }
}