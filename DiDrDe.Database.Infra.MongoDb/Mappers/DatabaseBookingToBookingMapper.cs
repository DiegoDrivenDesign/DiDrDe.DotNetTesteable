﻿using DiDrDe.Database.Infra.MongoDb.Model;
using DiDrDe.DotNetTesteable.Application.Contracts;
using DiDrDe.DotNetTesteable.Domain;

namespace DiDrDe.Database.Infra.MongoDb.Mappers
{
    public class DatabaseBookingToBookingMapper
        : IMapper<DatabaseBooking, Booking>
    {
        public Booking Map(DatabaseBooking source)
        {
            var id = source.Id;
            var name = source.Name;
            var booking = new Booking(id, name);
            return booking;
        }
    }
}