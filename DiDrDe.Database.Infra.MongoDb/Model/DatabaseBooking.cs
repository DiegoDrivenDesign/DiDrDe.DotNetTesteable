﻿using DiDrDe.Database.Infra.MongoDb.Contracts;
using System;

namespace DiDrDe.Database.Infra.MongoDb.Model
{
    public class DatabaseBooking
        : IMongoDbPersistable
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}