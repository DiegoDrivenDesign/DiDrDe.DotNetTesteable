﻿using System;

namespace DiDrDe.Database.Infra.MongoDb.Contracts
{
    public interface IMongoDbPersistable
    {
        Guid Id { get; }
    }
}