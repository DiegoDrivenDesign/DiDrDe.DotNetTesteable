﻿using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.DotNetTesteable.WebApiApp.UnitTests.ProgramTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private Program _sut;

            protected override void Given()
            {
            }

            protected override void When()
            {
                _sut = new Program();
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }
        }
    }
}