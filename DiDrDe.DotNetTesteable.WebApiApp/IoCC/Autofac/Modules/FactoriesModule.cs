﻿using Autofac;
using DiDrDe.DotNetTesteable.Application.Contracts;
using DiDrDe.DotNetTesteable.Application.Factories;

namespace DiDrDe.DotNetTesteable.WebApiApp.IoCC.Autofac.Modules
{
    public class FactoriesModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<IdFactory>()
                .As<IIdFactory>()
                .SingleInstance();

            builder
                .RegisterType<DomainBookingFactory>()
                .As<IDomainBookingFactory>()
                .SingleInstance();
        }
    }
}